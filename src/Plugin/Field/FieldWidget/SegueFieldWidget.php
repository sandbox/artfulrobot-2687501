<?php

/**
 * @file
 * Contains \Drupal\segue\Plugin\Field\FieldWidget\SegueFieldWidget.
 */

namespace Drupal\segue\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Component\Utility\Html;


/**
 * Plugin implementation of the 'segue_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "segue_field_widget",
 *   label = @Translation("Segue"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class SegueFieldWidget extends LinkWidget {
  /**
   * {@inheritdoc}
   *
   * Adds an empty 'classes' default value.
   */
  public static function defaultSettings() {
    return array(
      'classes' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * Set up the classes required.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['classes'] = array(
      '#type' => 'textarea',
      '#rows' => '5',
      '#cols' => '50',
      '#title' => t('Enter classes available'),
      '#default_value' => $this->getSetting('classes'),
      '#required' => TRUE,
      '#title' => 'Classes available',
      '#description' => 'Enter classes one per line, classname, semi-colon, friendly description like: primary;This is the primary link',
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $classes = $this->parseClassesSetting();
    $summary[] = "Classes: " . implode(', ', array_keys($classes));
    return $summary;
  }
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $items[$delta];

    $element['classes'] = array(
      '#type' => 'checkboxes',
      '#default_value' => !empty($item->options['attributes']['class']) ? $item->options['attributes']['class'] : [],
      '#options' => $this->parseClassesSetting(),
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * Does as LinkWidget but adds classes, to be stored in the options array.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['uri'] = static::getUserEnteredStringAsUri($value['uri']);
      $value['options']['attributes']['class'] = array_filter($value['classes']);
    }
    return $values;
  }

  /**
   * Parse classes setting.
   *
   * Classes are entered in a textarea like:
   *
   *     foo;Foo is for me
   *     bar;Bar is another option
   *
   * This function just splits these into an array:
   *
   *     'foo' => 'Foo is for me',
   *     'bar' => 'Bar is another option',
   *
   * @return array
   */
  protected function parseClassesSetting() {
    $parsed = [];
    foreach (explode("\n", $this->getSetting('classes')) as $line) {
      // Split on first ;
      preg_match('/^([^;]+);(.*)$/', $line, $matches);
      if (!preg_match('/^[a-zA-Z_0-9- ]+$/', $matches[1])) {
        // Doesn't look right, ignore it. @todo move this to settings form
        // validation.
        continue;
      }
      $parsed[trim($matches[1])] = Html::escape($matches[2]);
    }
    return $parsed;
  }

}
